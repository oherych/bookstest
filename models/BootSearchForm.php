<?php

namespace app\models;

use Yii;
use yii\base\Model;

class BootSearchForm extends Model
{
    public $name;
    public $author_id;
    public $fromDate;
    public $toDate;

    public function rules()
    {
        return [
            [['author_id'], 'integer'],
            [['name', 'fromDate', 'toDate'], 'string', 'max' => 255]
        ];
    }
}