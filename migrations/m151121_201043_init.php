<?php

use yii\db\Schema;
use yii\db\Migration;
use app\models\User;

class m151121_201043_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING . ' NOT NULL',
            'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . ' NOT NULL',

            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);

        $this->createTable('{{%authors}}', [
            'id' => Schema::TYPE_PK,
            'firstname' => Schema::TYPE_STRING . ' NOT NULL',
            'lastname' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->createTable('{{%books}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'preview' => Schema::TYPE_STRING . ' NOT NULL',
            'date' => Schema::TYPE_DATE . ' NOT NULL',
            'author_id' => Schema::TYPE_INTEGER . ' NOT NULL',

            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);

        $this->addForeignKey('books_author_id', '{{%books}}', 'author_id', '{{%authors}}', 'id');

		// Test data

        $demoUser = new User();
        $demoUser->username = 'demo';
        $demoUser->setPassword('pass');
        $demoUser->generateAuthKey();
        $demoUser->save();


        for($j = 1; $j <= 10; $j++) {
            $this->insert('authors', [
                'firstname' => 'author firstname ' . $j,
                'lastname' => 'author lastname ' . $j,
            ]);
        }
        
    }

    public function down()
    {
        $this->dropTable('{{%books}}');
        $this->dropTable('{{%authors}}');
        $this->dropTable('{{%user}}');
    }
}
