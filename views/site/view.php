<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Books */

$this->title = $model->name;

$author = $model->getAuthor()->one();
?>
<div class="books-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'preview',
            'date',
            [
                'attribute'=>'author_id',
                'value' => ( $author ? $author->firstname . ' ' . $author->lastname : '')
            ],
            'created_at:date',
            'updated_at:date',
        ],
    ]) ?>

</div>
