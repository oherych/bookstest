<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Authors;
use dosamigos\datepicker\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\web\View;
use branchonline\lightbox\Lightbox;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
?>
<div class="books-index">

    <?php $form = ActiveForm::begin(['method' => 'get']); ?>
    <div class="row">
        <div class="col-xs-3">
            <?= $form->field($filterModel, 'author_id')
                ->dropDownList(ArrayHelper::map(Authors::find()->all(), 'id', 'firstname'), ['prompt'=> $filterModel->getAttributeLabel('author_id')])
                ->label(false)
            ?>
        </div>
        <div class="col-xs-3">
            <?= $form->field($filterModel, 'name')
                ->textInput(['maxlength' => true, 'placeholder' => $filterModel->getAttributeLabel('name')])
                ->label(false)
            ?>
        </div>
    </div>
    <div class="row">

        <div class="col-xs-2">
            Дата виходу книжки
        </div>

        <div class="col-xs-2">
            <?= $form->field($filterModel, 'fromDate')->widget(DatePicker::classname(), [
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label(false) ?>
        </div>
        <div class="col-xs-2">
            <?= $form->field($filterModel, 'toDate')->widget(DatePicker::classname(), [
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label(false) ?>
        </div>
        

        <div class="col-xs-1 pull-right">
            <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    'id',
                    'name',
                    [
                        'attribute'=>'preview',
                        'format' => 'raw',
                        'contentOptions' => [
                            'class' => 'td-preview'
                        ],
                        'value' => function($data) {
                            if($data->preview) {

                                return Lightbox::widget([
                                    'files' => [
                                        [
                                            'thumb' => $data->preview,
                                            'original' => $data->preview,
                                            'title' => $data->name,
                                        ],
                                    ]
                                ]);
                            }
                        },
                    ],
                    [
                        'attribute'=>'author_id',
                        'format' => 'text',
                        'value' => function($data) {
                            $author = $data->getAuthor()->one();
                            
                            if($author) {
                                return $author->firstname . ' ' . $author->lastname;
                            }
                        },
                    ],
                    'date',
                    'created_at:date',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'view' => function($url, $model, $key ) {
                                $options = [
                                    'title' => Yii::t('yii', 'View'),
                                    'aria-label' => Yii::t('yii', 'View'),
                                    'class' => 'view-action',
                                ];
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                            }
                        ]

                    ],
                ],
            ]); ?>
        </div>
    </div>

    <p>
        <?= Html::a('Create Books', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

</div>


<?php Modal::begin([
      'header' => '<h2>View</h2>',
      'id' => 'view-modal',
]); ?>
 ddfdf
<?php Modal::end(); 

$this->registerJs('
jQuery(function($) {
    $(".view-action").click(function() {
        var url = $(this).attr("href");

        $.get(url, function(data) {
            $("#view-modal").find(".modal-body").html(data)
                            .end().modal();
        });

        return false;
    });
});
', View::POS_END, 'GridView-view-modal');

$this->registerCss('
    .td-preview img {
        max-width: 100px;
    }
');